from http.server import BaseHTTPRequestHandler, HTTPServer
import images
from urllib.parse import parse_qs


class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('content-type', 'text/html')
        self.end_headers()

        params = parse_qs(self.path[2:])
        function = params['func'][0]
        uid = params['uid'][0]

        if function == "1":
            image = images.Images(uid)
            url = image.getRecommendedImage()
            self.wfile.write(url.encode())
        elif function == "2":
            rating = params['rating'][0]
            imageId = params['iid'][0]
            images.Images(uid).logImage(imageId, rating)


def main():
    PORT = 8089
    server_address = ('localhost', PORT)
    server = HTTPServer(server_address, RequestHandler)
    print("Server running on port %s" % PORT)
    server.serve_forever()


if __name__ == '__main__':
    main()

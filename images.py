import numpy as np
import pandas as pd
from surprise import Dataset, Reader
from surprise import KNNBasic
from collections import defaultdict
from operator import itemgetter
import heapq
from csv import DictWriter


class Images:
    userId = ""
    userViewImages = ''
    imageData = ''

    def __init__(self, uid):
        self.userId = int(uid)
        self.userViewImages = pd.read_csv(r'/Users/devinr/HF/user_view_images.csv')
        self.imageData = pd.read_csv(r'/Users/devinr/HF/image_data.csv')
        self.userViewImages['userId'] = self.userViewImages['userId'].apply(int)

    def logImage(self, imageId, rating):

        field_names = ['userId', 'image_id', 'rating']
        userViewImagesDict = {'userId': self.userId, 'image_id': imageId, 'rating': rating}

        if rating == 5:
            with open('/Users/devinr/HF/user_view_images.csv', 'a', newline='') as userViewImageFile:
                writer = DictWriter(userViewImageFile, fieldnames=field_names)
                writer.writerow(userViewImagesDict)
                userViewImageFile.close()
        else:
            updatedFrame = self.userViewImages
            row = np.where((updatedFrame["image_id"] == int(imageId))
                                & (updatedFrame["userId"] == self.userId))[0]
            updatedFrame.loc[row[0], 'rating'] = int(rating)
            updatedFrame.to_csv('/Users/devinr/HF/user_view_images.csv', index=False)

    def getNewImage(self):
        image = self.imageData.sample()
        url = image.iloc[0]["url"]
        imageId = image.iloc[0]["image_id"]

        while (self.userViewImages["userId"] == self.userId).any() \
                and (self.userViewImages["image_id"] == imageId).any():
            image = self.imageData.sample()
            url = image.iloc[0]["url"]
            imageId = image.iloc[0]["image_id"]

        self.logImage(imageId, 5)
        return url

    def getRecommendedImage(self):
        if (self.userViewImages["userId"] == self.userId).any():
            recommendations = self.getImageRecommendations()
            if not recommendations:
                return self.getNewImage()
            else:
                for imageId in recommendations:
                    if (self.userViewImages["userId"] == self.userId).any() \
                            and (self.userViewImages["image_id"] == imageId).any():
                        print(str(imageId) + " imageId has been viewed by user " + str(self.userId))
                        continue
                    else:
                        url = self.imageData.loc[self.imageData['image_id'] == imageId].iloc[0]["url"]
                        self.logImage(imageId, 5)
                        return url
            print("all images in recommendations have been viewed by the user")
            return self.getNewImage()
        else:
            print("New user: " + str(self.userId))
            return self.getNewImage()

    def getImageRecommendations(self):
        modelReader = Reader(rating_scale=(1, 10))
        ratingData = Dataset.load_from_df(self.userViewImages[['userId', 'image_id', 'rating']], modelReader)
        ratingTrainset = ratingData.build_full_trainset()
        rating_user_matrix = KNNBasic(sim_options={
            'name': 'cosine',
            'user_based': False
        }) \
            .fit(ratingTrainset) \
            .compute_similarities()

        uid = self.userId
        sid = ratingTrainset.to_inner_uid(uid)
        userRatings = ratingTrainset.ur[uid]
        k_neighbors = heapq.nlargest(5, userRatings, key=lambda t: t[1])
        matches = defaultdict(float)

        for uid, rating in k_neighbors:
            try:
                similarities = rating_user_matrix[sid]
                for iid, mScore in enumerate(similarities):
                    matches[iid] += mScore * (rating / 5.0)
            except:
                continue
        imageRecommendations = []
        seenImages = {}
        position = 0

        for imageId, rating in ratingTrainset.ur[uid]:
            seenImages[imageId] = 1

        for imageId, sumRating in sorted(matches.items(), key=itemgetter(1), reverse=True):
            if not imageId in seenImages:
                imageRecommendations.append(ratingTrainset.to_raw_iid(imageId))
                position += 1
                if position > 2:
                    break
        return imageRecommendations
